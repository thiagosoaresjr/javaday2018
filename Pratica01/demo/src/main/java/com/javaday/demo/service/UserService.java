package com.javaday.demo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.javaday.demo.model.User;

@Service
public class UserService {

	private Map<String, User> users;
	
    public UserService() {
		super();
		users = new HashMap<>();
		users.put("981649828", new User("Thiago Soares", "981649828"));
	}

	public void saveUser(User user) {
        users.put(user.getPhoneNumber(), user);
    }

    public User findUserByPhoneNumber(String phoneNumber) {
    	System.out.println(users);
        return users.get(phoneNumber);
    }

}
